import ssh2 from 'ssh2'
const { Client } = ssh2

/**
 * @description Permet de gérer les actions ssh sur le proxy
 * @class ProxyManager
 */
class ProxyManager {

    /**
     * @description Initialise le ProxyManager avec les données d'authentification.
     * @param {string} PROXY_ADR adresse vers la machine proxy
     * @param {string} PROXY_LOGIN nom d'utilisateur pour se connecter en ssh
     * @param {string} PROXY_PASSWORD mot de passe de l'utilisateur ssh
     * @memberof ProxyManager
     */
    constructor(PROXY_ADR, PROXY_LOGIN, PROXY_PASSWORD) {
        this.proxyAdress = PROXY_ADR
        this.proxyLogin = PROXY_LOGIN
        this.proxyPassword = PROXY_PASSWORD
    }

    /**
     * @description Permet d'écrire dans un fichier ligne par ligne sur le proxy
     * @param {Array<string>} content liste de ligne du fichier a écrire
     * @param {string} path chemin vers le fichier
     * @memberof ProxyManager
     */
    async writeFile(content, path) {
        let commands = ""
        content.forEach((line, index) => {
            if (index === 0) {
                commands += `echo "${line}" > ${path}\n`
            }
            else {
                commands += `echo "${line}" >> ${path}\n`
            }
        });
        await this.sendCommand(commands)
    }

    /**
     * @description Permet d'envoyer une commande au proxy
     * @param {string} commands command a envoyer
     * @memberof ProxyManager
     */
    async sendCommand(commands) {
        const conn = new Client();
        commands += "exit\n"
        conn.on('ready', function () {
            conn.shell(function (err, stream) {
                if (err) throw err;
                stream.on('close', function () {
                    conn.end();
                })
                stream.end(commands);
            });
        }).connect({
            host: this.proxyAdress,
            username: this.proxyLogin,
            password: this.proxyPassword
        });
    }


}

export default ProxyManager