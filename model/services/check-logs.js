import CustomLogger, { LoggerType } from "../logger.js";
import env from '../../.env.js'

/**
 * Deletes files that do not respect the retention rule
 */
export default function checkLogs() {
  const logger = new CustomLogger(env.LOGS_FOLDER, env.LOGS_RETENTION)
  logger.log(LoggerType.INFO, "Starting the checkLogs service...");
  try {
    logger.clean();
  } 
  catch (err) {
    logger.log(LoggerType.WARN, `Error during checkLogs service: ${err}`);
  }
}
