import fs from "fs";
import chalk from "chalk";

/**
 * Type de logs disponibles
 */
export const LoggerType = {
  INFO: "INFO",
  WARN: "WARN",
  ERROR: "ERROR",
};


/**
 * @description Permet de générer et supprimer les fichiers de logs
 * @class CustomLogger
 */
class CustomLogger {

  /**
   * @description CustomLogger constructor
   * @param logsFolder Chemin relatif vers le dossier de logs
   * @param retention  Nombre de jour de rétention
   * @memberof CustomLogger
   */
  constructor(logsFolder, retention) {
    this.logsFolder = logsFolder;
    this.retention = retention;
  }

  /**
   * @description Permet d'écrire et formater un log
   * @param {LoggerType} type Type de logs disponibles
   * @param {string} message Contenu du log
   * @memberof CustomLogger
   */
  log(type, message) {
    const date = new Date();
    const data = `${date.toISOString()} [${type}] ${message}`;

    fs.appendFile(this.getLoggerFile(date), data + "\n", (err) => {
      if (err) console.error(err);
    });

    switch (type) {
      case LoggerType.INFO:
        console.log(chalk.cyan(data));
        break;
      case LoggerType.WARN:
        console.log(chalk.yellow(data));
        break;
      case LoggerType.ERROR:
        console.log(chalk.red(data));
        break;
      default:
        console.log(chalk.magenta(data));
    }
  }

  /**
   * @description Supprime les fichiers qui ne suivent pas la règle de rétention
   * @memberof CustomLogger
   */
  clean() {
    const logsFiles = fs
      .readdirSync(this.logsFolder)
      .filter((file) => file.endsWith(".log"));

    const retentionFiles = [];
    const date = new Date();

    for (let i = 0; i < this.retention; i++) {
      retentionFiles.push(this.getLoggerFile(date));
      date.setDate(date.getDate() - 1);
    }

    for (const index in logsFiles) {
      const path = `${this.logsFolder}/${logsFiles[index]}`;
      if (retentionFiles.indexOf(path) !== -1) {
        this.log(LoggerType.INFO, `Service checkLogs > Kept ${path}`);
      } else {
        this.log(LoggerType.INFO, `Service checkLogs > Remove ${path}`);
        fs.unlink(path, (err) => {
          if (err)
            this.log(
              LoggerType.ERROR,
              `Service checkLogs > Error while deleting the file: ${err}`
            );
        });
      }
    }
  }

  /**
   * @description Génère le chemin du fichier de log pour une date donnée
   * @param {Date} date Date du fichier
   * @return Le chemin du fichier pour la date donnée
   * @memberof CustomLogger
   */
  getLoggerFile(date) {
    let month = (date.getMonth() + 1).toString();
    if (month.length === 1) month = "0" + month;

    let day = date.getDate().toString();
    if (day.length === 1) day = "0" + day;

    return `${this.logsFolder}/${date.getFullYear()}_${month}_${day}.log`;
  }
}

export default CustomLogger;
