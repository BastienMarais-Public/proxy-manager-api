import ovh from 'ovh'

/**
 * @description Permet de gérer les différents appels sur l'api ovh
 * @class OVHManager
 */
class OVHManager {

    /**
     * @description Initialise le OVHManager avec les données d'authentification.
     * @param {string} APP_KEY
     * @param {string} APP_SECRET
     * @param {string} CONSUMER_KEY
     * @memberof OVHManager
     */
    constructor(APP_KEY, APP_SECRET, CONSUMER_KEY) {
        this.client = new ovh({
            appKey: APP_KEY,
            appSecret: APP_SECRET,
            consumerKey: CONSUMER_KEY
        });
    }

    /**
     * @description Récupère les informations du compte associé
     * @return {Promise} 
     * @memberof OVHManager
     */
    async getMe() {
        return this.client.requestPromised('GET', '/me')
    }

    /**
     * @description Renvoie la liste des domaines possédés
     * @return {Promise} 
     * @memberof OVHManager
     */
    async getDomain() {
        return this.client.requestPromised('GET', '/domain')
    }

    /**
     * @description Permet d'ajouter une entrée dns
     * @param {string} zoneName nom de domaine
     * @param {string} fieldType CNAME | A | etc.
     * @param {string} subDomain nom du sous domaine
     * @param {string} target cible sur laquelle pointer
     * @return {Promise} 
     * @memberof OVHManager
     */
    async addDNSEntry(zoneName, fieldType, subDomain, target) {
        return this.client.requestPromised('POST', `/domain/zone/${zoneName}/record`, {
            "fieldType": fieldType,
            "subDomain": subDomain,
            "target": target
        })
    }

    /**
     * @description Renvoie la liste des id d'entrée dns d'un domaine
     * @param {string} zoneName nom de domaine 
     * @param {string} fieldType CNAME | A | etc.
     * @return {Promise} 
     * @memberof OVHManager
     */
    async getDNSEntries(zoneName, fieldType) {
        return this.client.requestPromised('GET', `/domain/zone/${zoneName}/record`, { "fieldType": fieldType })
    }

    /**
     * @description Récupère les informations d'une entrée dns a partir de son id
     * @param {string} zoneName non de domaine 
     * @param {string} id id de l'entrée
     * @return {Promise} 
     * @memberof OVHManager
     */
    async getDNSEntryById(zoneName, id) {
        return this.client.requestPromised('GET', `/domain/zone/${zoneName}/record/${id}`)
    }

    /**
     * @description Supprime une entrée dns a partir de son id
     * @param {string} zoneName non de domaine
     * @param {string} id id de l'entrée
     * @return {Promise} 
     * @memberof OVHManager
     */
    async removeDNSEntryById(zoneName, id) {
        return this.client.requestPromised('DELETE', `/domain/zone/${zoneName}/record/${id}`)
    }

    /**
     * @description Réactualise le dns pour le nom de domaine demandé
     * @param {string} zoneName non de domaine
     * @return {string} 
     * @memberof OVHManager
     */
    async refreshDNS(zoneName) {
        return this.client.requestPromised('POST', `/domain/zone/${zoneName}/refresh`)
    }
}


export default OVHManager