import express from 'express'
import bodyParser from 'body-parser'
import Backend from '../business/backend.js'
import env from '../.env.js'
import Customlogger, { LoggerType } from '../model/logger.js'

const backend = new Backend(env)
const logger = new Customlogger(env.LOGS_FOLDER, env.LOGS_RETENTION)
const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));


app.use(function (_req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE')
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    )
    next()
})


/*********************************************************
 * GET
 ********************************************************/

app.get('/api/validate', async (req, res) => {
    const { usagePassword } = req.query
    if (backend.checkUsagePassword(usagePassword)) {
        res.status(204).end();
        logger.log(LoggerType.INFO, "Mot de passe d'usage correct")
    }
    else {
        res.status(403).end();
        logger.log(LoggerType.WARN, "Mot de passe d'usage incorrect")
    }

})

app.get('/dns/domains', async (req, res) => {
    const { usagePassword } = req.query
    if (backend.checkUsagePassword(usagePassword)) {
        try {
            res.status(200).json(await backend.getDomains());
            logger.log(LoggerType.INFO, "Récupération de la liste des domaines")
        }
        catch (err) {
            res.status(500).end();
            logger.log(LoggerType.ERROR, `GET /dns/domains ${JSON.stringify(err)}`)
        }
    }
    else {
        res.status(403).end();
        logger.log(LoggerType.WARN, "Mot de passe d'usage incorrect")
    }

})

app.get('/dns/:domain/:fieldType', async (req, res) => {
    const { usagePassword } = req.query
    const { domain, fieldType } = req.params
    if (backend.checkUsagePassword(usagePassword)) {
        try {
            res.status(200).json(await backend.getDNSEntries(domain, fieldType));
            logger.log(LoggerType.INFO, `Récupération des entrées dns de type ${fieldType} sur le domaine ${domain}`)
        }
        catch (err) {
            res.status(500).end();
            logger.log(LoggerType.ERROR, `GET /dns/${domain}/${fieldType} ${JSON.stringify(err)}`)
        }
    }
    else {
        res.status(403).end();
        logger.log(LoggerType.WARN, `GET /dns/${domain}/${fieldType} Mot de passe d'usage incorrect`)
    }
})

/*********************************************************
 * POST
 ********************************************************/

app.post("/dns/:domain/refresh", async (req, res) => {
    const { usagePassword } = req.query
    const { domain } = req.params
    if (backend.checkUsagePassword(usagePassword)) {
        try {
            res.status(200).json(await backend.refreshDNS(domain));
            logger.log(LoggerType.INFO, `Réactualisation du dns sur le domaine ${domain}`)
        }
        catch (err) {
            res.status(500).end();
            logger.log(LoggerType.ERROR, `POST /dns/${domain}/refresh ${JSON.stringify(err)}`)
        }
    }
    else {
        res.status(403).end();
        logger.log(LoggerType.WARN, `POST /dns/${domain}/refresh Mot de passe d'usage incorrect`)
    }
})

app.post("/dns/create/entry", async (req, res) => {
    const { usagePassword } = req.query
    if (backend.checkUsagePassword(usagePassword)) {
        const { mainDomain, subDomain, fieldType, ovhTarget } = req.body
        try {
            res.status(200).json(await backend.createDNSEntry(mainDomain, fieldType, subDomain, ovhTarget));
            logger.log(LoggerType.INFO, `Création d'une entrée dns de type ${fieldType} pour ${subDomain}.${mainDomain} pointant vers ${ovhTarget}`)
        }
        catch (err) {
            res.status(500).end();
            logger.log(LoggerType.ERROR, `POST /dns/create/entry ${JSON.stringify(req.body)} ${JSON.stringify(err)}`)
        }
    }
    else {
        res.status(403).end();
        logger.log(LoggerType.WARN, `POST /dns/create/entry ${JSON.stringify(req.body)} Mot de passe d'usage incorrect`)
    }
})

app.post("/site/create", async (req, res) => {
    const { usagePassword } = req.query
    if (backend.checkUsagePassword(usagePassword)) {
        const { mainDomain, subDomain, fieldType, ovhTarget, proxyIp, proxyPort, ssl } = req.body
        try {
            res.status(200).json(await backend.createSite(mainDomain, subDomain, fieldType, ovhTarget, proxyIp, proxyPort, ssl));
            logger.log(LoggerType.INFO, `Création du site ${subDomain}.${mainDomain} de type ${fieldType} pointant vers ${ovhTarget} redirigeant lui même sur ${proxyIp}:${proxyPort} ${ssl === "true" ? "avec ssl" : "sans ssl"}`)
        }
        catch (err) {
            res.status(500).end();
            logger.log(LoggerType.ERROR, `POST /site/create ${JSON.stringify(req.body)} ${JSON.stringify(err)}`)
        }
    }
    else {
        res.status(403).end();
        logger.log(LoggerType.WARN, `POST /site/create ${JSON.stringify(req.body)} Mot de passe d'usage incorrect`)
    }
})

/*********************************************************
 * DELETE
 ********************************************************/

app.delete("/site/delete", async (req, res) => {
    const { usagePassword } = req.query
    if (backend.checkUsagePassword(usagePassword)) {
        const { mainDomain, subDomain, fieldType, ssl } = req.body
        try {
            res.status(200).json(await backend.deleteSite(mainDomain, subDomain, fieldType, ssl));
            logger.log(LoggerType.INFO, `Suppression du site ${subDomain}.${mainDomain} de type ${fieldType} ${ssl ? "avec ssl" : "sans"}`)

        }
        catch (err) {
            res.status(500).end();
            logger.log(LoggerType.ERROR, `DELETE /site/delete ${JSON.stringify(req.body)} ${JSON.stringify(err)}`)
        }
    }
    else {
        res.status(403).end();
        logger.log(LoggerType.WARN, `DELETE /site/delete ${JSON.stringify(req.body)} Mot de passe d'usage incorrect`)
    }
})

app.delete("/dns/delete/entry", async (req, res) => {
    const { usagePassword } = req.query
    if (backend.checkUsagePassword(usagePassword)) {
        const { mainDomain, entryId } = req.body
        try {
            res.status(200).json(await backend.deleteDNSEntry(mainDomain, entryId));
            logger.log(LoggerType.INFO, `Suppression d'une entrée dns d'id ${entryId} sur ${mainDomain}`)

        }
        catch (err) {
            res.status(500).end();
            logger.log(LoggerType.ERROR, `DELETE /dns/delete/entry ${JSON.stringify(req.body)} ${JSON.stringify(err)}`)
        }
    }
    else {
        res.status(403).end();
        logger.log(LoggerType.WARN, `DELETE /dns/delete/entry ${JSON.stringify(req.body)} Mot de passe d'usage incorrect`)
    }
})


export default app;