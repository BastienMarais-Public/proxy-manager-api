const env = Object.freeze({
    API_PORT: 8081,
    APP_KEY:"XXXXXXXX", 
    APP_SECRET:"XXXXXXXXXXXXXXX",
    CONSUMER_KEY:"XXXXXXXXXXXXXX",
    PROXY_ADR: "192.X.X.X",
    PROXY_LOGIN: "username",
    PROXY_PASSWORD: "mot de passe du proxy",
    USAGE_PASSWORD: "mot de passe de l'application",
    USAGE_PASSWORD_FAILED_LIMIT: 3,
    LOGS_FOLDER: "./logs",
    LOGS_RETENTION: 3
})

export default env
