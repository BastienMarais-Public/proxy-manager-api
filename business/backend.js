import OVHManager from '../model/ovhManager.js'
import ProxyManager from '../model/proxyManager.js'
import crypto from 'crypto'

/**
 * @description Classe intermédiare entre le model et l'api
 * @class Backend
 */
class Backend {

    /**
     * @description Initialise l'ovh manager et le proxy manager avec les variables d'authentifications
     * @param {{APP_KEY:string, APP_SECRET:string, CONSUMER_KEY:string, PROXY_ADR:string, PROXY_LOGIN:string, PROXY_PASSWORD:string, USAGE_PASSWORD:string, USAGE_PASSWORD_FAILED_LIMIT:number}} env variables pour le paramétrage
     * @memberof Backend
     */
    constructor(env) {
        this.ovhManager = new OVHManager(env.APP_KEY, env.APP_SECRET, env.CONSUMER_KEY)
        this.proxyManager = new ProxyManager(env.PROXY_ADR, env.PROXY_LOGIN, env.PROXY_PASSWORD)
        this.usagePassword = crypto.createHash('sha256').update(env.USAGE_PASSWORD).digest('hex').toUpperCase()
        this.usagePasswordFailedLimit = env.USAGE_PASSWORD_FAILED_LIMIT

        this.lastLoginFailed = new Date().getTime()
        this.usagePasswordFailedCounter = 0.
    }

    /****************************************
     * ACCESS
     ****************************************/

    /**
     * @description vérifie si le mdp donné est bon et vérifie si on ne dépasse pas les limites d'erreurs
     * @param {string} usagePassword sha256 en base 64
     * @memberof Backend
     */
    checkUsagePassword(usagePassword) {

        const now = new Date().getTime()
        const usagePasswordIsOk = usagePassword === this.usagePassword

        // Si on a déjà trop d'erreur dans les dernières 15 mins où si le mdp est faux
        if (((this.usagePasswordFailedCounter >= this.usagePasswordFailedLimit) && ((now - this.lastLoginFailed) < (15 * 60 * 1000))) || !usagePasswordIsOk){
            this.lastLoginFailed = now
            this.usagePasswordFailedCounter += 1
            return false
        }

        // Si le mdp est bon on remet le compteur a zéro
        this.usagePasswordFailedCounter = 0
        return true
    }

    /****************************************
     * SITE
     ****************************************/

    /**
     * @description Créer les fichiers de conf du site et créer son entrée dns et optionnellement ajoute le certificat ssl
     * @param {string} mainDomain nom de domaine ex: domain.fr
     * @param {string} subDomain sous domaine ex: test
     * @param {string} fieldType CNAME | A | AAAA etc.
     * @param {string} ovhTarget cible dns souhaitée dans OVH
     * @param {string} proxyIp ip locale sur laquelle redirigé les requêtes
     * @param {string} proxyPort port de la machine locale 
     * @param {boolean} ssl utilisation de l'https via certbot sur le site
     * @memberof Backend
     */
    async createSite(mainDomain, subDomain, fieldType, ovhTarget, proxyIp, proxyPort, ssl) {
        await this.createDNSEntry(mainDomain, fieldType, subDomain, ovhTarget)
        const content = [
            `server {`,
            `  # NGINX will listen on port 80 for both IP V4 and V6`,
            `  listen 80;`,
            `  listen [::]:80;`,
            ` `,
            `  # Here we should specify the name of server`,
            `  server_name ${subDomain}.${mainDomain};`,
            ` `,
            `  # Requests to given location will be redirected`,
            `  location / {`,
            ` `,
            `    # NGINX will pass all requests to specified location here`,
            `    proxy_pass http://${proxyIp}:${proxyPort}/;`,
            `    proxy_set_header X-Forwarded-For \\$proxy_add_x_forwarded_for;`,
            `    proxy_set_header X-Forwarded-Proto \\$scheme;`,
            `    proxy_set_header X-Forwarded-Port \\$server_port;`,
            `  }`,
            `}`
        ]
        const path = `/etc/nginx/sites-available/${subDomain}.${mainDomain}`
        await this.proxyManager.writeFile(content, path)
        await this.proxyManager.sendCommand(`ln -s /etc/nginx/sites-available/${subDomain}.${mainDomain} /etc/nginx/sites-enabled/ \n`)
        await this.proxyManager.sendCommand(`systemctl restart nginx\n`)

        if (ssl === "true") {
            setTimeout(async () => {
                await this.proxyManager.sendCommand(`certbot --nginx -n --domains "${subDomain}.${mainDomain}" --redirect\n`)
                await this.proxyManager.sendCommand(`systemctl restart nginx\n`)
            }, 10000)
        }
        return "ok"
    }

    /**
     * @description Supprime les fichiers nginx du site, annule son certificat ssl et supprime son entrée dns
     * @param {string} mainDomain nom de domaine ex: domain.fr
     * @param {string} subDomain sous domaine ex: test
     * @param {string} fieldType CNAME | A | AAAA etc.
     * @param {boolean} ssl utilisation de l'https via certbot sur le site
     * @memberof Backend
     */
    async deleteSite(mainDomain, subDomain, fieldType, ssl) {
        const DNSentries = await this.getDNSEntries(mainDomain, fieldType)
        for (let i = 0; i < DNSentries.length; i++) {
            if (DNSentries[i].subDomain === subDomain && DNSentries[i].zone === mainDomain && DNSentries[i].fieldType === fieldType) {
                await this.deleteDNSEntry(mainDomain, DNSentries[i].id)
            }
        }
        await this.ovhManager.refreshDNS(mainDomain)
        let path = `/etc/nginx/sites-available/${subDomain}.${mainDomain}`
        await this.proxyManager.sendCommand(`rm -f ${path}\n`)
        path = `/etc/nginx/sites-enabled/${subDomain}.${mainDomain}`
        await this.proxyManager.sendCommand(`rm -f ${path}\n`)
        if (ssl === "true") {
            await this.proxyManager.sendCommand(`certbot revoke -n --cert-name "${subDomain}.${mainDomain}"\n`)
        }
        await this.proxyManager.sendCommand(`systemctl restart nginx\n`)
    }


    /****************************************
     * DNS
     ****************************************/

    /**
     * @description Renvoie tous les domaines possédés
     * @memberof Backend
     */
    async getDomains() {
        return (await this.ovhManager.getDomain()).sort()
    }

    /**
     * @description Renvoie toutes les entrées dns du domaine et de type demandé
     * @param {string} zoneName nom de domaine ex: domain.fr
     * @param {string} fieldType CNAME | A | AAAA etc.
     * @memberof Backend
     */
    async getDNSEntries(zoneName, fieldType) {
        const DNSentriesId = await this.ovhManager.getDNSEntries(zoneName, fieldType)
        let DNSentries = []
        for (let i = 0; i < DNSentriesId.length; i++) {
            DNSentries.push(await this.ovhManager.getDNSEntryById(zoneName, DNSentriesId[i]))
        }
        return DNSentries.sort()
    }

    /**
     * @description Demande la réactualisation du DNS pour le domaine demandé
     * @param {string} mainDomain nom de domaine
     * @memberof Backend
     */
    async refreshDNS(mainDomain) {
        await this.ovhManager.refreshDNS(mainDomain)
    }

    /**
     * @description Créer une entrée dns fieldType au format subDomain.mainDomain vers ovhTarget
     * @param {string} mainDomain nom de domaine ex: domain.fr 
     * @param {string} fieldType CNAME | A | AAAA etc.
     * @param {string} subDomain sous domaine ex: test 
     * @param {string} ovhTarget cible dns souhaitée dans OVH 
     * @memberof Backend
     */
    async createDNSEntry(mainDomain, fieldType, subDomain, ovhTarget) {
        if (!(await this.existDNSEntry(mainDomain, subDomain, fieldType, ovhTarget))) {
            await this.ovhManager.addDNSEntry(mainDomain, fieldType, subDomain, ovhTarget)
            await this.ovhManager.refreshDNS(mainDomain)
        }
    }

    /**
     * @description Supprime l'entrée dns entryId dans le mainDomain
     * @param {string} mainDomain nom de domaine ex: domain.fr 
     * @param {string} entryId id de l'entrée dns
     * @memberof Backend
     */
    async deleteDNSEntry(mainDomain, entryId) {
        if (!isNaN(Number(entryId))) {
            await this.ovhManager.removeDNSEntryById(mainDomain, Number(entryId))
            await this.ovhManager.refreshDNS(mainDomain)
        }
    }

    /**
     * @description Vérifie si une entrée existe déjà ou non
     * @param {string} mainDomain nom de domaine ex: domain.fr 
     * @param {string} subDomain sous domaine ex: test 
     * @param {string} fieldType CNAME | A | AAAA etc.
     * @param {string} ovhTarget cible dns souhaitée dans OVH  
     * @memberof Backend
     */
    async existDNSEntry(mainDomain, subDomain, fieldType, ovhTarget) {
        const entries = await this.getDNSEntries(mainDomain, fieldType)
        for (let i = 0; i < entries.length; i++) {
            if (entries[i].subDomain === subDomain && entries[i].zone === mainDomain && entries[i].fieldType === fieldType && entries[i].target === ovhTarget) {
                return true
            }
        }
        return false
    }

}


export default Backend
