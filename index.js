import app from './api/app.js'
import env from './.env.js'
import checkLogs from "./model/services/check-logs.js";

const port = env.API_PORT

checkLogs();
setInterval(checkLogs, 24 * 60 * 60 * 1000); // Chaque jour

const server = app.listen(port, () => {
    const socketInfo = server.address()
    let startupMessage = "[HTTP-SERVER] Server started."

    if (typeof socketInfo === "object" && socketInfo?.address) {
        if (socketInfo.address === "::") {
            startupMessage += ` http://localhost:${socketInfo.port}`
        }
        else {
            startupMessage += ` http://${socketInfo.address}:${socketInfo.port}`
        }
    }
    else if (typeof socketInfo === "string") {
        startupMessage += ` Pipe name or Unix domain socket name: ${socketInfo}`
    }

    console.log(startupMessage)
})
