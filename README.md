# proxy-manager-api

Cette API a pour but de simplifier le déploiement de site web avec https.
## Pré-requis

Pour que l'application fonctionne bien vous devez :
* Utiliser OVH comme fournisseur de domaine
* Rediriger vos flux 80 et 443 de router vers une machine proxy
  * Cette machine doit avoir nginx
  * Cette machine doit avoir certbot + une première authentification pour initialiser le nom et l'email
## Installation 

```sh
apt-get update -y
apt-get install -y git
curl -sL https://deb.nodesource.com/setup_18.x | bash -
apt-get install -y nodejs
git clone https://gitlab.com/BastienMarais-Public/proxy-manager-api.git
cd proxy-manager-api
// FAIRE LA CONFIGURATION DU FICHIER .ENV.JS AVANT LA SUITE
npm i -P
npm start
```

Pour faire fonctionner l'application vous avez besoin compléter le fichier `.env.js` tel que :
```js
const env = Object.freeze({
    API_PORT: 8081,
    APP_KEY:"XXXXXXXX", 
    APP_SECRET:"XXXXXXXXXXXXXXX",
    CONSUMER_KEY:"XXXXXXXXXXXXXX",
    PROXY_ADR: "192.X.X.X",
    PROXY_LOGIN: "username",
    PROXY_PASSWORD: "mot de passe du proxy",
    USAGE_PASSWORD: "mot de passe de l'application",
    USAGE_PASSWORD_FAILED_LIMIT: 3,
    LOGS_FOLDER: "./logs",
    LOGS_RETENTION: 3
})

export default env
```

| Clé                         | Description                                                                                                                                                                                        |
| :-------------------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| API_PORT                    | Port sur lequel l'api sera disponible                                                                                                                                                              |
| APP_KEY & APP_SECRET        | Pour obtenir APP_KEY et APP_SECRET, il faut remplir le formulaire suivant : https://eu.api.ovh.com/createApp/                                                                                      |
| CONSUMER_KEY                | Pour obtenir CONSUMER_KEY il faut utiliser la commande `Get Consumer Key` sur POSTMAN qui vous donnera un lien dans `validationUrl` qui vous redirigera vers un formulaire en ligne.               |
| PROXY_ADR                   | Adresse ip de la machine proxy                                                                                                                                                                     |
| PROXY_LOGIN                 | Nom d'utilisateur qui doit se connecter au proxy via ssh (root n'y est pas autorisé par défaut))                                                                                                   |
| PROXY_PASSWORD              | Mot de passe de l'utilisateur ci-dessus                                                                                                                                                            |
| USAGE_PASSWORD              | Mot de passe pour exécuter une commande de l'api /!\ dans le fichier env.js vous devez marquer le mdp brute mais lorsque vous l'utiliser dans l'api il faut son hash(sha256) en hexa et majuscules |
| USAGE_PASSWORD_FAILED_LIMIT | Nombre d'échecs toléré dans les 15 minutes                                                                                                                                                         |
| LOGS_FOLDER                 | Dossier où seront stockés les fichiers de logs                                                                                                                                                     |
| LOGS_RETENTION              | Nombre de fichiers de logs conservés                                                                                                                                                               |


Une fois que vous avez rempli ces variables il vous suffit de taper :
```sh
npm i -P 
npm start 
```

## Fonctionnalités 

Une fois l'application installée vous pouvez :
* Créer et supprimer une entrée dns dans vos domaines 
* Lancer une réactualisation du dns pour un domaine donné
* Créer un site 
  * Création des fichiers de config nginx
  * Création de l'entrée dns sur ovh
  * Activation du certificat https via certbot si demandé
* Supprimer un site
  * Suppression des fichiers de config nginx
  * Suppression de l'entrée dns sur ovh
  * Désactivation du certificat https via certbot si demandé

Une collection postman est a votre disposition pour taper directement dessus.
